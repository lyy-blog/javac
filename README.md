### JAVAC编译原理

1：The Scanner class maps source text into a stream of tokens. The JavacParser class converts the stream of tokens into one or more syntax trees.  
2:一个源文件会被编译成JCCompilationUnit 
3:语法糖:泛型,自动装箱，拆箱，foreach

### 语法树大分类
1. JCCompilationUnit :一个java原文件
2. JCImport :import语句
3. JCMethodDecl :方法声明
4. JCCatch :catch 代码块
5. JCStatement :语句声明
6. JCExpression :表达式
7. JCTypeParameter :类参数类型
8. TypeBoundKind :类接口直接绑定类别 extends, super, exact, or unbound
9. JCModifiers :修饰符

### 词法分析
##### 相关类
* JavacParser,ParserFactory,Scanner,ScannerFactory,JavaTokenizer,Token
##### 流程
* 有哪些字符  UnicodeReader
* 哪些是token的开始字符
* 哪些是token的结束字符
#### 疑问
* isJavaIdentifierPart 表示什么
### 语法分析
#### 相关类
* JavacParser,TreeMaker,JCTree
### 流程
* 构建JCCompilationUnit
### 语义分析